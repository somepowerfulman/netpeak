<?php


namespace App\config;


class DBConnection
{
    private $dbConnection;

    public function __construct($host, $port, $db_name, $user, $pass)
{
    try {
        $this->dbConnection = new \PDO("mysql:host=$host;port=$port;dbname=$db_name", $user,$pass);

    } catch (\PDOException $e){
        exit($e->getMessage());
    }

}
    public function getDbConnection()
    {
        return $this->dbConnection;
    }


}