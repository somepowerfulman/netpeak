<?php
namespace App\apiClient;

use GuzzleHttp\Client;

class BaseApiClient
{
    private $api_url;
    protected $client;

    public function __construct($api_url)
    {
        $this->api_url = $api_url;
        $this->client = new Client();
    }

    public function request($method, $url, $params = null)
    {
        $options = [];
        if ($params != null){
            $options['query'] = $params;
        }

        $response = $this->client->request($method, $this->api_url.$url, $options);
        return json_decode($response->getBody());
    }
}

