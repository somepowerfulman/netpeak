<?php
namespace App\apiClient;

class ExchangeratesapiClient extends BaseApiClient
{
    public function getCurrencyRate($base_currency, $to_currency)
    {
        $params = [
                'base' => $base_currency,
                'symbols' => $to_currency
        ];

        return $this->request('GET', '/latest', $params);
    }
}