<?php require_once 'lauout/head.html'; ?>
<div class="container">
    <div class="row mt-3">
        <a class="btn btn-success mb-2" href="/">Calculator</a>
        <h2 style="margin: 0 auto;">History</h2>

    </div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">From</th>
            <th scope="col">To</th>
            <th scope="col">From amount</th>
            <th scope="col">To amount</th>
            <th scope="col">Rate</th>
            <th scope="col">Date</th>
        </tr>
        </thead>

        <?php
        foreach ($response as $row) {
            ?>

            <tbody>
            <tr>
                <th scope="row"><?= $row['id'] ?></th>
                <td><?= $row["from_cur"] ?></td>
                <td><?= $row["to_cur"] ?></td>
                <td><?= $row["from_amount"] ?></td>
                <td><?= $row["to_amount"] ?></td>
                <td><?= $row["rate"] ?></td>
                <td><?= $row["created_at"] ?></td>
            </tr>

            </tbody>
            <?php
        }
        ?>
    </table>
    <nav aria-label="Page navigation">
        <ul class="pagination">
            <?php if(isset($prev)){?><li class="page-item"><a class="page-link" href="history?page_number=<?=$prev?>">Previous</a></li><?php }?>
            <?php if(isset($next)){?><li class="page-item"><a class="page-link" href="history?page_number=<?=$next?>">Next</a></li><?php }?>
        </ul>
    </nav>

    <a class="btn btn-danger" href="settings">settings</a>

</div>
</div>
</body>
</html>





