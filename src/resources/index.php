<?php require_once 'lauout/head.html';?>
<div class="container mt-4">
    <div class="row center-block">
        <form id="form" style="margin: 0 auto;" action="calculate" class="form-control">
            <div class="input-group ">
                <div class="col-2"></div>
                <div class="col-3">
                    <select name="currencyFrom" class="custom-select">
                        <option selected>Choose currency</option>
                        <?php
                        foreach ($response as $currency){
                            ?>
                            <option value="<?=$currency['value']?>"><?=$currency['value'] ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <input type="text" class=" mt-3 form-control" placeholder="Enter amount" id="amount" name="amount">
                </div>
                <div class="col-3">
                    <select name="currencyTo" class="custom-select">
                        <option selected>Choose currency</option>
                        <?php
                        foreach ($response as $currency){
                            ?>
                            <option value="<?=$currency['value']?>"><?=$currency['value'] ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <input disabled type="text" class="form-control mt-3" placeholder="0" id="result" name="result">

                </div>
                <div class="col-2">
                        <button class="w-100 btn btn-primary" type="submit">calculate</button>
                    <div class="row">
                        <div class=">col-6 ml-2"><a href="history" class="mt-3 w-100 btn btn-warning" type="button">history</a></div>
                        <div class=">col-6 ml-2"><a href="settings" class="mt-3 w-100 btn btn-secondary" type="button">settings</a></div>
                    </div

                </div>
                <div class="ml-3">

                </div>
            </div>
        </form>
    </div>

    <div class="row mt-3" id="response">

    </div>
</div>

<script>
    $(document).ready(function () {
        $("#form").submit(function () {
            event.preventDefault();
            console.log('ok');
            $.ajax({
                url: "calculate",
                type: "POST",
                data: $("form").serialize(),
                processData: false,
                success: function (response) {
                    $("#result").val(response.response.sum);

                },
                error: function (response) { // Данные не отправлены
                    $("#result").html('<h5 style = "background: #dc3545; border-radius: 5px; text-align: center; min-height: 30px; margin-top: 20px">' + ERROR + "</h5>");
                }
            })
        })
    })
</script>
</body>
</html>
