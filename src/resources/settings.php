<?php require_once 'lauout/head.html';
//var_dump($response);
?>
<div class="container">
    <div class="row mt-3">
        <a class="btn btn-success mb-2" href="/">Calculator</a>
        <h2 style="margin: 0 auto;">Settings</h2>
<!--        <a class="btn btn-primary" href="Go to calc">settings</a>-->

    </div>
    <form id="form" action="settings" method="PATCH">
    <label for="record_pp">Count records<input type="text" name="record_pp"></label>
    <h4>Currencies</h4>
        <?php
        foreach ($response as $currency){
            if ($currency['name'] == 'currency'){


        ?>
    <div class="form-check">
        <input value="1" name="<?=$currency['value']?>" type="checkbox" class="form-check-input" id="<?=$currency['value']?>">
        <label class="form-check-label" for="exampleCheck1"><?=$currency['value']?></label>
    </div>
        <?php
        }
        }
        ?>

    <button type="submit" class="btn btn-success">save</button>
        <a href="history" class="btn btn-warning" type="button">history</a>
    </form>
    <div class="col-3" id="message"></div>
</div>
</div>
<script>
    $(document).ready(function () {
        $("#form").submit(function () {
            event.preventDefault();
            console.log('ok');
            $.ajax({
                url: "settings",
                type: "PATCH",
                data: $("form").serialize(),
                processData: false,
                success: function (response) {
                    $("#message").html('<h5 style = "background: #0adc05; width = 50px; border-radius: 5px; text-align: center; min-height: 30px; margin-top: 20px">saved</h5>');
                },
                error: function (response) { // Данные не отправлены
                    $("#message").html('<h5 style = "background: #dc3545; border-radius: 5px; text-align: center; min-height: 30px; margin-top: 20px">something wrong </h5>');
                }
            })
        })
    })
</script>
</body>
</html>