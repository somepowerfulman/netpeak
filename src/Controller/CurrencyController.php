<?php


namespace App\Controller;


use App\apiClient\ExchangeratesapiClient;
use App\Model\CurrencyModel;

class CurrencyController extends BaseController
{
    private $currency_api_client;

    public function __construct($dataBase, $requestMethod, ExchangeratesapiClient $currency_api_client)
    {
        parent::__construct($dataBase, $requestMethod);
        $this->currency_api_client = $currency_api_client;
    }

    protected function processGet()
    {
        $model = $this->model();
        $response = $model->findAll(0, 20);
        require_once('../src/resources/index.php');
    }

    protected function processPost()
    {
        $base_currency = $_POST["currencyFrom"];
        $to_currency = $_POST["currencyTo"];
        $response = $this->currency_api_client->getCurrencyRate($base_currency, $to_currency);
        $rate = $response->rates->$to_currency;
        $sum = $this->calculate($rate, $_POST["amount"]);

//transaction history
        $input = ['from_cur' => $base_currency,
            'to_cur' => $to_currency,
            'from_amount' => $_POST["amount"],
            'to_amount' => $sum,
            'rate' => $rate];
        $model = $this->model();
        $model->insert($input);
        $response = array("response" => ['sum' => $sum,
            'from' => $base_currency,
            "to" => $to_currency,
            'from_amount' => $_POST["amount"],
            'to_amount' => $sum,
            'rate' => $rate]);
        header($response['status_code_header']);
        header("Content-Type: application/json; charset=UTF-8");
        echo json_encode($response);


    }

    public function calculate($rate, $amount)
    {
        return round($rate * $amount, 2);
    }

    public function model()
    {
        return new CurrencyModel($this->dataBase);
    }

}