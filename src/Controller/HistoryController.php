<?php


namespace App\Controller;


use App\Model\SettingsModel;
use App\Model\TransactionHistoryModel;

class HistoryController extends BaseController
{

    protected function processGet()
    {
        $model = $this->model();
        $settings = new SettingsModel($this->dataBase);
        $page_size = $settings->getByName('record_pp');
        $response = $model->findAll($_GET['page_number'] ?? 0, $page_size[0]['value']);
        if (count($response) >= $page_size[0]['value']){
            $next = $_GET['page_number'] + $page_size[0]['value'];
        }
        if ($_GET['page_number'] > 0){
            $prev = ($_GET['page_number']<$page_size[0]['value']) ? 0 :$_GET['page_number'] - $page_size[0]['value'];
        }
        require_once ('../src/resources/history.php');
    }
    public function model()
    {
        return new TransactionHistoryModel($this->dataBase);
    }
}