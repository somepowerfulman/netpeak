<?php
namespace App\Controller;

use GuzzleHttp\Client;

class BaseController
{
    protected $dataBase;
    protected $requestMethod;


    public function __construct($dataBase, $requestMethod)
{
    $this->dataBase = $dataBase;
    $this->requestMethod = $requestMethod;
}

public function processRequest()
{
    switch ($this->requestMethod){
        case 'GET':
             $this->processGet();
             break;
        case "POST":
            $this->processPost();
            break;
        case "PATCH":
            $this->processPatch();
            break;
        default:
            $this->notFound();
    }
}

    protected function processPost()
    {
        $this->notFound();
    }

    protected function processGet()
    {
        $this->notFound();
    }

    protected function processPatch()
    {
        $this->notFound();
    }

    protected function notFound()
    {
        header("HTTP/1.1 404 Not Found");
        exit();
    }



}
