<?php


namespace App\Controller;


use App\Model\SettingsModel;

class SettingsController extends BaseController
{

    public function processGet()
    {
        $model = $this->model();
        $response = $model->findAll(0, 20);
        require_once '../src/resources/settings.php';
    }

    public function processPatch()
    {
        parse_str(file_get_contents('php://input'), $_PATCH);
        $model = $this->model();
        $response = $model->multiPatchFromQuery($_PATCH);
    }


    public function model()
    {
        return new SettingsModel($this->dataBase);
    }
}