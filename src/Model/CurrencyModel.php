<?php


namespace App\Model;


class CurrencyModel extends BaseModel
{

    protected function getFindAllQuery()
    {

        return "SELECT currency.settings.value
                FROM currency.settings 
                where currency.settings.is_active = 1 and currency.settings.name = 'currency'";
    }

    public function findAll($offset, $page_size)
    {
        $statement = $this->getFindAllQuery();
        $result = $this->runQuery($statement);
        return $result;
    }

    public function prepareInsertData(array $input): array
    {
        return array('transaction_history',
            [
                [
                    'from_cur' => $input['from_cur'],
                    'to_cur' => $input['to_cur'],
                    'from_amount' => $input['from_amount'],
                    'to_amount' => $input['to_amount'],
                    'rate' => $input['rate']
                ]
            ]);
    }

}