<?php


namespace App\Model;


abstract class BaseModel
{
    private $db = null;
    protected $tableName;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function getDb()
    {
        return $this->db;
    }

//    abstract protected function getFindAllQuery($page_size, $page_number);

    public function findAll($offset, $page_size)
    {
        $limit = $page_size;
        $statement = $this->getFindAllQuery($limit, $offset);
        $result = $this->runQuery($statement);
        return $result;
    }

    public function findItem($id)
    {
        $statement = $this->getFindItemQuery();
        $result = $this->runQuery($statement, array($id));
        return $this->prepare_result($result);
    }

    public function runQuery(string $statement, array $query_args = null)
    {

        try {
            $statement = $this->getDb()->prepare($statement);
            $statement->execute($query_args);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    function pdoMultiInsert($tableName, $data, $pdoObject)
    {

        //Will contain SQL snippets.
        $rowsSQL = array();

        //Will contain the values that we need to bind.
        $toBind = array();

        //Get a list of column names to use in the SQL statement.
        $columnNames = array_keys($data[0]);

        //Loop through our $data array.
        foreach ($data as $arrayIndex => $row) {
            $params = array();
            foreach ($row as $columnName => $columnValue) {
                $param = ":" . $columnName . $arrayIndex;
                $params[] = $param;
                $toBind[$param] = $columnValue;
            }
            $rowsSQL[] = "(" . implode(", ", $params) . ")";
        }

        //Construct our SQL statement
        $sql = "INSERT INTO `$tableName` (" . implode(", ", $columnNames) . ") VALUES " . implode(", ", $rowsSQL);

        //Prepare our PDO statement.
        $pdoStatement = $pdoObject->prepare($sql);

        //Bind our values.
        foreach ($toBind as $param => $val) {
            $pdoStatement->bindValue($param, $val);
        }
        //Execute our statement (i.e. insert the data).
        return $pdoStatement->execute();
    }


    public function prepareInsertData(array $input): array
    {

    }

    public function insert(array $input)
    {
        $prepared_data = $this->prepareInsertData($input);
        try {
            $this->getDb()->beginTransaction();
            $this->pdoMultiInsert($prepared_data[0], $prepared_data[1], $this->getDb());

            $item_id = $this->getDb()->lastInsertId();
            $this->getDb()->commit();
        } catch (\PDOException $e) {
            $this->getDb()->rollBack();
            exit($e->getMessage());
        }
    }


}


