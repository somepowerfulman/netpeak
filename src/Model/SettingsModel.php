<?php


namespace App\Model;


class SettingsModel extends BaseModel
{
    protected $tableName = 'settings';

    protected function getFindAllQuery()
    {

        return "SELECT  
                    currency.settings.name,
                    currency.settings.value,
                    currency.settings.is_active
                FROM $this->tableName";
    }

    public function getByName($name)
    {
        $statement = $this->getFindAllQuery() . " where name = ?;";

        return $this->runQuery($statement, array($name));
    }

    public function multiPatchFromQuery($input)
    {
        $sql = $this->getDb()->prepare("update settings set settings.value = :record_pp where settings.name = 'record_pp'");
        $sql->bindParam(":record_pp", $input['record_pp']);
        $sql->execute();
        unset($input['record_pp']);
        $statement = "SELECT currency.settings.value FROM $this->tableName where name = 'currency'";
        $statement = $this->runQuery($statement);
        foreach ($statement as $key => $record) {
            $sql = $this->getDb()->prepare("update settings set settings.is_active = 0 where settings.value = :record");
            $sql->bindParam(":record", $record["value"]);
            $sql->execute();

        }

        foreach ($input as $k => $v) {
            $sql = $this->getDb()->prepare("update settings set settings.is_active = 1 where settings.value = :k");
            $sql->bindParam(":k", $k);
            $sql->execute();

        }
    }

}