<?php


namespace App\Model;


class TransactionHistoryModel extends BaseModel
{
    protected $tableName = 'transaction_history';

    public function findAll($offset, $page_size)
    {

        $limit = $page_size;
        $statement = $this->getFindAllQuery($limit, $offset);

        $result = $this->runQuery($statement);
        return $result;
    }
    protected function getFindAllQuery($page_size, $page_number)
    {
        return "SELECT
                    transaction_history.from_cur,
                    transaction_history.to_cur,
                    transaction_history.from_amount,
                    transaction_history.to_amount,
                    transaction_history.rate,
                    transaction_history.created_at
        FROM $this->tableName order by transaction_history.created_at desc LIMIT $page_size OFFSET $page_number;";
    }




}