<?php
require dirname(__DIR__).'/../vendor/autoload.php';
use App\config\DBConnection;
$db_connection = (new DBConnection('db',3306,'currency','db_user','db_password'))-> getDbConnection();
$tables = <<<EOS
drop table transaction_history;
drop table settings;

CREATE TABLE IF NOT EXISTS settings (
id SMALLINT auto_increment not null primary key,
name varchar(16) NOT NULL,
value varchar(128) NOT NULL,
type varchar (16) default 'str',
is_active boolean default false);

INSERT INTO settings
(name, value, type, is_active)
VALUES
('currency', 'USD', 'str', true),
('currency', 'EUR', 'str', true),
('currency', 'GBP', 'str', true),
('currency', 'KRW', 'str', true),
('currency', 'RUB', 'str', true),
('record_pp', '20', 'int', true);


CREATE TABLE IF NOT EXISTS transaction_history (
id SMALLINT auto_increment not null primary key,
from_cur varchar(16) NOT NULL,
to_cur varchar(16) NOT NULL,
from_amount decimal (16,2) NOT NULL,
to_amount decimal (16,2) NOT NULL,
rate decimal (11,10),
created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP);
EOS;

try {
    $db_connection ->exec($tables);
    echo "Success!\n";
}
catch (\PDOException $e) {
    exit($e->getMessage());
}
