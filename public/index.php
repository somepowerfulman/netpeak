<?php

use App\apiClient\ExchangeratesapiClient;
use App\config\DBConnection;
use App\Controller\CurrencyController ;
use App\Controller\HistoryController;
use App\Controller\SettingsController;

require '../vendor/autoload.php';
$db_connection = (new DBConnection('db',3306,'currency','db_user','db_password'))-> getDbConnection();
$currency_api_client = new ExchangeratesapiClient('https://api.exchangeratesapi.io');
$uri = trim($_SERVER['REQUEST_URI'],'/');
$uri = explode("/", $uri);
$uri_params = explode('?', $uri[0]);
$requestMethod = $_SERVER['REQUEST_METHOD'];

switch ($uri_params[0]){
    case '':
        $controller = new CurrencyController($db_connection,$requestMethod, $currency_api_client);
//require_once '../src/resources/index.php';
    break;
    case 'calculate':
        $controller = new CurrencyController($db_connection,$requestMethod, $currency_api_client);
        break;
    case 'history':
        $controller = new HistoryController($db_connection,$requestMethod);
        break;
    case 'settings':
        $controller = new SettingsController($db_connection,$requestMethod);
        break;
    default:
        header("HTTP/1.1 404 Not Found");
        exit();
}
if (isset($controller)) {
    $controller->processRequest();
}





